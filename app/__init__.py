# database.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:root@localhost/myDbName"
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://mclkim:root1234@192.168.10.10:3307/stock"

db = SQLAlchemy(app)
#
#
# # create app
# def create_app():
#     db = SQLAlchemy(app)
#     db.init_app(app)
#     db.create_all()
#     return app


from app import views, models
