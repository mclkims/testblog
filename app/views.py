from flask import render_template
from app import app
from .models import Post


@app.route('/hello')
def hello_world():
    return 'Hello World!'


@app.route('/login')
def login():
    return 'Hello World!'


@app.route('/')
def show_entries():
    entries = Post.query.all()
    return render_template('show_entries.html', entries=entries)
